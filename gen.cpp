#include <cassert>
#include <iostream>
#include <cstdio>
#include <string>

#include "linalg.h"

void gen_rhs(int n, alglib::complex_2d_array &b ) {
	for (int i = 0; i < n; ++i) {
		b[i][0].x = rand() / ((double) RAND_MAX);
		b[i][0].y = rand() / ((double) RAND_MAX);
	}
}

int main(int argc, char **argv)
{
  //const std::vector<double> x { 0.606531,0.670320,0.740818,0.818731,0.904837,1.000000,1.105171,1.221403,1.349859,1.491825,1.648721 };
  //
  /*
    if (argc < 2) {
        printf("which N?\n");
    }
    int n = atoi(argv[1]);
    char fname[30] = "matrix";
    FILE* f;
    strcat(fname, argv[1]);
    printf("%s\n", fname);
    alglib::complex_2d_array a;
    a.setlength(n,n);
    alglib::cmatrixrndcond(n, 10.0, a);
    f = fopen(fname, "w");
    for (int i = 0; i < n; ++i) {
	for (int j = 0; j < n; ++j) {
	    fprintf(f, "%lf %lf ", a[i][j].x, a[i][j].y);
	}
	fprintf(f, "\n");
    }
    fclose(f);
    
    //printf("%lf\n", a[1][1].y);
*/
    if (argc < 2) {
        printf("which N?\n");
    }
    int n = atoi(argv[1]);
    alglib::complex_2d_array b;
    b.setlength(n, 1);
    alglib::complex_2d_array err;
    err.setlength(1, 1);
	
	gen_rhs(n, b);
	alglib::cmatrixgemm(1, 1, n, 1.0, b, 0,0,2,b,0,0,0,0,err,0,0);
	printf("%20.17lf\n", err[0][0].x);
	printf("%20.17lf\n", sqrt(err[0][0].x));
    printf("ZBS!\n");
    return 0;
}
