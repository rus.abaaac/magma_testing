#include <cassert>
#include <iostream>
#include <cstdio>
#include <string>

#include "linalg.h"

#include "cublas_v2.h"
#include "magma_v2.h"      
#include "magma_lapack.h"
#include "magma_operators.h"
//#include "magma_timer.h"


void cfill_matrix(
    magma_int_t m, magma_int_t n, magmaFloatComplex *A, 
    alglib::complex_2d_array &a,
    magma_int_t lda )
{
    #define A(i_, j_) A[ (i_) + (j_)*lda ]
    
    magma_int_t i, j;
    for (j=0; j < n; ++j) {
        for (i=0; i < m; ++i) {
            A(i,j) = MAGMA_C_MAKE(  float(a[i][j].x),    // real part
                                    float(a[i][j].y) );  // imag part
        }
    }
    
    #undef A
}

void cfill_matrix_alglib(
    magma_int_t m, magma_int_t n, magmaFloatComplex *A, 
    alglib::complex_2d_array &a,
    magma_int_t lda )
{
    #define A(i_, j_) A[ (i_) + (j_)*lda ]
    
    magma_int_t i, j;
    for (j=0; j < n; ++j) {
        for (i=0; i < m; ++i) {
            a[i][j].x = (double) real(A(i,j));
            a[i][j].y = (double) imag(A(i,j));
        }
    }
    
    #undef A
}

// ------------------------------------------------------------
// Replace with your code to initialize the dA matrix on the GPU device.
// This simply leverages the CPU version above to initialize it to random values,
// and copies the matrix to the GPU.
void cfill_matrix_gpu(
    magma_int_t m, magma_int_t n, magmaFloatComplex *dA, magma_int_t ldda,
    alglib::complex_2d_array &a,
    magma_queue_t queue )
{
    magmaFloatComplex *A;
    magma_int_t lda = ldda;
    magma_cmalloc_cpu( &A, m*lda );
    if (A == NULL) {
        fprintf( stderr, "malloc failed\n" );
        return;
    }
    cfill_matrix( m, n, A, a, lda );

    magma_csetmatrix( m, n, A, lda, dA, ldda, queue );
    magma_free_cpu( A );
}


// ------------------------------------------------------------
// Replace with your code to initialize the dX rhs on the GPU device.
void cfill_rhs_gpu(
    magma_int_t m, magma_int_t nrhs, magmaFloatComplex *dX, magma_int_t lddx,
    alglib::complex_2d_array &a,
    magma_queue_t queue )
{
    cfill_matrix_gpu( m, nrhs, dX, lddx, a, queue );
}

void cget_sol_gpu(
    magma_int_t m, magma_int_t n, magmaFloatComplex *dA, magma_int_t ldda,
    alglib::complex_2d_array &a,
    magma_queue_t queue ) 
{
    magmaFloatComplex *A;
    magma_int_t lda = ldda;
    magma_cmalloc_cpu( &A, m*lda );
    if (A == NULL) {
        fprintf( stderr, "malloc failed\n" );
        return;
    }
    magma_cgetmatrix ( m , n , dA , m , A , m, queue );
    cfill_matrix_alglib ( m, n, A, a, lda );
    magma_free_cpu ( A );
}

// ------------------------------------------------------------
// Solve dA * dX = dB, where dA and dX are stored in GPU device memory.
// Internally, MAGMA uses a hybrid CPU + GPU algorithm.
void gpu_interface( magma_int_t n, magma_int_t nrhs, alglib::complex_2d_array &a, 
    alglib::complex_2d_array &b, 
    alglib::complex_2d_array &x )
{
    magmaFloatComplex *dA=NULL, *dX=NULL;
    magma_int_t *ipiv=NULL;
    magma_int_t ldda = magma_roundup( n, 32 );  // round up to multiple of 32 for best GPU performance
    magma_int_t lddx = ldda;
    magma_int_t info = 0;
    magma_queue_t queue=NULL;
    magma_int_t dev = 0;
    double start,  mid, end;
    double gpu_time;
    
    // magma_*malloc routines for GPU memory are type-safe,
    // but you can use cudaMalloc if you prefer.
    magma_cmalloc( &dA, ldda*n );
    magma_cmalloc( &dX, lddx*nrhs );
    magma_imalloc_cpu( &ipiv, n );  // ipiv always on CPU
    if (dA == NULL || dX == NULL || ipiv == NULL) {
        fprintf( stderr, "malloc failed\n" );
        goto cleanup;
    }
    
    magma_queue_create( dev, &queue );
    
    // Replace these with your code to initialize A and X
    cfill_matrix_gpu( n, n, dA, ldda, a, queue );
    cfill_rhs_gpu( n, nrhs, dX, lddx, b, queue );
   
    //start = get_current_time(); 
    start = magma_wtime(); 
 
    magma_cgetrf_gpu(n, n, dA, n, ipiv, &info);
    mid = magma_wtime(); 
    magma_cgetrs_gpu(MagmaNoTrans, n, nrhs, dA, ldda, ipiv, dX, ldda, &info);
    if (info != 0) {
        fprintf( stderr, "magma_cgetrs_gpu failed with info=%d\n", info );
    }

    end = magma_wtime(); 
    gpu_time = (end - start);
    printf("only cgetrs GPU time: %10.8lf sec.\n", end-mid);
    printf("all GPU time: %10.8lf sec.\n", gpu_time);
    
    cget_sol_gpu(n, nrhs, dX, lddx, x, queue );
    // TODO: use result in dX
    
cleanup:
    magma_queue_destroy( queue );
    magma_free( dA );
    magma_free( dX );
    magma_free_cpu( ipiv );
}

void gen_right_side( int n, int nrhs, alglib::complex_2d_array &b ) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < nrhs; ++j) {
            b[i][j].x = rand() / ((double) RAND_MAX);
            b[i][j].y = rand() / ((double) RAND_MAX);
        }
    }
}

double get_residual( alglib::complex_2d_array &a,
    alglib::complex_2d_array &x,
    alglib::complex_2d_array &b, int n )
{
    alglib::complex_2d_array err;
    err.setlength(1,1);
    /*
    alglib::complex_1d_array x;
    alglib::complex_1d_array b;
    b.setlength(n);
    x.setlength(n);
    for (int i = 0; i < n; ++i) {
        b[i].x = bb[i][0].x;
        b[i].y = bb[i][0].y;
        x[i].x = xx[i][0].x;
        x[i].y = xx[i][0].y;
    }
*/
    alglib::cmatrixgemm ( n, 1, n, 1.0, a, 0, 0, 0, x, 0, 0, 0, -1.0, b, 0, 0 );
    alglib::cmatrixgemm ( 1, 1, n, 1.0, b, 0, 0, 2, b, 0, 0, 0, 0, err, 0, 0);
    return err[0][0].x;
}

int main(int argc, char **argv)
{
  //const std::vector<double> x { 0.606531,0.670320,0.740818,0.818731,0.904837,1.000000,1.105171,1.221403,1.349859,1.491825,1.648721 };
  //
    if (argc < 2) {
        printf("which N?\n");
    }
    int n = atoi(argv[1]);
    int nrhs = 1;
    double err;
    char fname[30] = "matrix";
    FILE* f;
    double xx, y;
    printf("N = %d\n", n);
    alglib::complex_2d_array a;
    alglib::complex_2d_array b;
    alglib::complex_2d_array x;
    a.setlength(n,n);
    b.setlength(n, nrhs);
    x.setlength(n, nrhs);
    if (argc == 2) {
	alglib::cmatrixrndcond(n, 10.0, a);
    } else {
	strcat(fname, argv[1]);
	f = fopen(fname, "r");
        for (int i = 0; i < n; ++i) {
	    for (int j = 0; j < n; ++j) {
		fscanf(f, "%lf %lf", &xx, &y);
		a[i][j].x = xx;
		a[i][j].y = y;
	    }
	}
	fclose(f);
    }
    gen_right_side(n, nrhs, b);
    
    magma_init();

    gpu_interface(n, nrhs, a, b, x);

    magma_finalize();

    err = get_residual(a, x, b, n);
    printf("Residual: %20.17lf\n", err);
    return 0;
}
